var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.get('/', function(req, res){
  res.sendfile('index.html');
});

var sockets = [],
    mob_desk_rel = [];

io.on('connection', function(socket){
  console.log('a user connected. socket:'); //console.log(socket);
  var code = String(Math.ceil(Math.random()*10000)); //код окна

  //принимаем код, проверяем, отпавляем результат
  socket.on('code',function(data){
    if (data.code){
      if (typeof(sockets[data.code]) !== 'undefined'){
        socket.emit('code', 'ok');
        mob_desk_rel[data.selfcode] = data.code; //запомним связь
      } else{
        socket.emit('code', 'fail');
      }
    }

    //принимаем команду на движение от одного и передаем другому сокету
    socket.on('move',function(data){
      console.log('move from '+data.selfcode);
      if (data.selfcode){
        if(typeof(mob_desk_rel[data.selfcode]) !== 'undefined'){
          var remote = mob_desk_rel[data.selfcode];
          console.log('remote target '+remote);
          sockets[remote].emit('move', data.moving);
        }
      }
    });

    console.log('code: accepted data: ' + data.code);
    //console.log('found id = '+sockets[data].id);

  });

  //при разъединении
  socket.on('disconnect', function(){
    for (code in sockets){//пройдемся по таблице сокетов - удалим свежеотсоединенный
        if (sockets[code].id === socket.id){
            delete sockets[code];
            console.log('socket id='+socket.id+' was deleted.')

            for (mob_code in mob_desk_rel){//пройдемся по связям - удалим разоравнную
                if (mob_desk_rel[mob_code] === code ){
                    delete mob_desk_rel[mob_code];
                    console.log('relation for '+mob_code+'was deleted.');
                }
            }
        }
    }
    console.log('user disconnected');
  });

  socket.emit('sock_code',code);//отправим код сокета клиенту
  sockets[code] = socket;
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});